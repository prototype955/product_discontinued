<!-- Template product_subscribers overrided by addon product_discontinued -->
<div class="btn-toolbar clearfix">
    {if $product_subscribers}
        <div class="pull-left">
            {if !$runtime.company_id || $runtime.company_id && $product_data.company_id == $runtime.company_id}
                {btn type="delete_selected" icon="icon-trash" dispatch="dispatch[products.update]" form="subscribers_form"}
            {/if}
        </div>
    {/if}
        <div class="pull-left">
            {include file="views/products/components/search_product_subscribers.tpl" dispatch="products.update" search=$product_subscribers_search}
        </div>
    {if !$runtime.company_id || $runtime.company_id && $product_data.company_id == $runtime.company_id}        
        <div class="pull-right">
            {capture name="new_email_picker"}
                <form action="{"products.update?product_id=`$product_id`&selected_section=subscribers"|fn_url}" method="post" name="subscribers_form_0" class=" ">
                    <div class="form-horizontal form-edit cm-tabs-content" id="content_tab_user_details">
                        <div class="control-group">
                            <label for="users_email" class="control-label cm-required cm-email">{__("email")}</label>
                            <div class="controls">
                                <input type="text" name="add_users_email" id="users_email" value="" class="span8" />
                                <input type="hidden" name="add_users[0]" id="users_id" value="0"/>
                            </div>
                        </div>
                    </div>
                <div class="buttons-container">
                    {include file="buttons/save_cancel.tpl" but_name="dispatch[products.update]" cancel_action="close"}
                </div>
                </form>
            {/capture}
            {capture name="send_alternative"}
                <form action="{"products.update?product_id=`$product_id`&selected_section=subscribers"|fn_url}" method="post" name="subscribers_form_send_alternative_product" class=" ">
                    <div class="form-horizontal form-edit cm-tabs-content" id="content_tab_send_alternative">
                        <input type="hidden" name="alternative_product_send" value="{"YesNo::YES"|enum}">
                        <div class="control-group">
                            <label for="alternative_product" class="control-label cm-required">{__("select_alternative_product")}</label>
                            <div class="controls">
                                {include 
                                    file="views/products/components/picker/picker.tpl"
                                    input_name="alternative_product[product]"
                                    item_ids=[]|array_filter
                                }
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="remove_subscribers_after_send">{__('remove_subscribers_after_send')}:</label>
                            <div class="controls">
                                <input type="hidden" name="alternative_product[remove_subscribers_after_send]" value="{"YesNo::NO"|enum}">
                                <input 
                                    id="remove_subscribers_after_send"
                                    type="checkbox" 
                                    name="alternative_product[remove_subscribers_after_send]" 
                                    value="{"YesNo::YES"|enum}" 
                                    checked="checked"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="buttons-container">
                        {include file="buttons/save_cancel.tpl" but_text=__("send") but_name="dispatch[products.update]" cancel_action="close"}
                    </div>
                </form>
            {/capture}
            <span class="shift-right">
            
                {if $product_data.is_discontinued === "YesNo::YES"|enum && !empty($product_subscribers)}
                    {include 
                        file="common/popupbox.tpl" 
                        id="send_alternative" 
                        text=__("send_alternative") 
                        content=$smarty.capture.send_alternative 
                        link_text=__("send_alternative") 
                        act="general"
                    }
                {/if}

                {include file="common/popupbox.tpl" id="add_new_subscribers" text=__("new_subscribers") content=$smarty.capture.new_email_picker link_text=__("add_subscriber") act="general" icon="icon-plus"}
            </span>

            {include file="pickers/users/picker.tpl" no_container=true data_id="subscr_user" picker_for="subscribers" extra_var="products.update?product_id=`$product_id`&selected_section=subscribers" but_text=__("add_subscribers_from_users") view_mode="button" but_meta="btn"}

        </div>
    {/if}
</div>
<form action="{""|fn_url}" method="post" name="subscribers_form" class="{if ""|fn_check_form_permissions || ($runtime.company_id && $product_data.shared_product == "Y" && $product_data.company_id != $runtime.company_id)} cm-hide-inputs{/if}">

{include file="common/pagination.tpl" save_current_page=true div_id="product_subscribers" search=$product_subscribers_search}

{if $product_subscribers}
<div class="table-responsive-wrapper">
    <table width="100%" class="table table-middle table--relative table-responsive">
    <thead>
        <tr>
        <th class="center" width="1%">
            {include file="common/check_items.tpl"}</th>
        <th width="50%">{__("email")}</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
        {foreach from=$product_subscribers item="s"}
        <tr>
            <td class="center" data-th="">
                   <input type="checkbox" name="subscriber_ids[]" value="{$s.subscriber_id}" class="cm-item" /></td>
            <td data-th="{__("email")}"><input type="hidden" name="subscribers[{$s.subscriber_id}][email]" value="{$s.email}" />
                <a href="mailto:{$s.email|escape:url}">{$s.email}</a></td>
                <input type="hidden" name="product_id" value="{$product_id}" />
            <td class="nowrap right" width="5%" data-th="{__("tools")}">
                {capture name="tools_list"}
                    <li>{btn type="delete" href="products.update?product_id=`$product_id`&selected_section=subscribers&deleted_subscription_id=`$s.subscriber_id`"}</li>
                {/capture}
                <div class="hidden-tools">
                {if !$runtime.company_id || $runtime.company_id && $product_data.company_id == $runtime.company_id}
                    {dropdown content=$smarty.capture.tools_list}
                {/if}
                </div>
            </td>
        </tr>
        {/foreach}
    </tbody>
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="product_subscribers" search=$product_subscribers_search}

</form>

<div class="control-group">
    <label for="elm_datafeed_exclude_discontinued_products" class="control-label">{__("exclude_discontinued_products")}:</label>
    <div class="controls"><input type="hidden" name="datafeed_data[params][exclude_discontinued_products]" value="{"YesNo::NO"|enum}" />
        <input 
            type="checkbox" 
            name="datafeed_data[params][exclude_discontinued_products]" 
            id="elm_datafeed_exclude_discontinued_products" 
            value="{"YesNo::YES"|enum}" 
            {if $datafeed_data.params.exclude_discontinued_products === "YesNo::YES"|enum}checked="checked"{/if} 
        />
    </div>
</div>
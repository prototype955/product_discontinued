{if $_REQUEST["product_id"]}
    {include file="common/subheader.tpl" title=__("discontinued") target="#acc_discontinued"}
    <div id="acc_discontinued">
        <div class="control-group">
            <label class="control-label" for="product_is_discontinued">{__("is_discontinued")}:</label>
            <div class="controls">
                <input type="hidden" name="product_data[is_discontinued]" id="product_is_discontinued" value="{"YesNo::NO"|enum}">
                <input 
                    type="checkbox" 
                    name="product_data[is_discontinued]" 
                    value="{"YesNo::YES"|enum}" 
                    {if $product_data.is_discontinued === "YesNo::YES"|enum} checked="checked" {/if}
                    onclick="Tygh.$.disable_elms(['discontinue_cause', 'discontinue_date', 'discontinue_action'], !this.checked);"
                />
            </div>
        </div>

        {if $product_data.is_discontinued !== "YesNo::YES"|enum}
            {$inputs_disabled = 'disabled="disabled"'}
        {else}
            {$inputs_disabled = false}
        {/if}

        <div class="control-group">
            <label class="control-label" for="discontinue_cause">{__("discontinue_cause")}:</label>
            <div class="controls">
                <input 
                    type="text" 
                    id="discontinue_cause" 
                    name="product_data[discontinue_cause]" 
                    value="{$product_data.discontinue_cause|default:__("default_discontinue_cause")}" 
                    {$inputs_disabled} 
                />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="discontinue_date">{__('discontinue_date')}:</label>
            <div class="controls">
                {include 
                    file="common/calendar.tpl" 
                    date_id="discontinue_date" 
                    date_name="product_data[discontinue_date]" 
                    extra=$inputs_disabled
                    date_val=$product_data.discontinue_date|default:$smarty.const.TIME 
                    start_year=$settings.Company.company_start_year
                }
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="discontinue_action">{__('discontinue_action')}:</label>
            <div class="controls">
                <input type="hidden" name="product_data[discontinue_action]" value="{"YesNo::NO"|enum}" {$inputs_disabled}>
                <input 
                    id="discontinue_action"
                    type="checkbox" 
                    name="product_data[discontinue_action]" 
                    value="{"YesNo::YES"|enum}" 
                    {if $product_data.discontinue_action === "YesNo::YES"|enum} checked="checked" {/if}
                    {$inputs_disabled} 
                />
            </div>
        </div>
    </div>
    {else}
    <input type="hidden" name="product_data[is_discontinued]" id="product_is_discontinued" value="{"YesNo::NO"|enum}">
    <input type="hidden" name="product_data[discontinue_cause]" id="discontinue_cause" value="{$product_data.discontinue_cause|default:__("default_discontinue_cause")}" >
    <input type="hidden" name="product_data[discontinue_date]" id="discontinue_date" value="{$product_data.discontinue_date|default:$smarty.const.TIME}" >
    <input type="hidden" name="product_data[discontinue_action]" id="discontinue_action" value="{"YesNo::NO"|enum}}" >
{/if}

{include file="common/letter_header.tpl"}

{__("product_discontinued.text_buy_with_one_click_request", ["[product_href]" => $product_url, "[product_name]" => $product_name, "[alt_product_href]" => $alt_product_href, "[alt_product_name]" => $alt_product_name, "[customer]" => $customer])} <br />

{include file="common/letter_footer.tpl"}
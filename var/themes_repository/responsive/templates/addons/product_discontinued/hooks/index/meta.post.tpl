{if isset($product) && $product.is_discontinued === "YesNo::YES"|enum && $settings.product_discontinued.general.hide_from_search === "YesNo::YES"|enum}
    <meta name="robots" content="noindex, nofollow" />
{/if}

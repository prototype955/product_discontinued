{if 
    $product.is_discontinued !== "YesNo::YES"|enum 
    || 
    (
        $settings.product_discontinued.general.display_price === "display" 
        || 
        $settings.product_discontinued.general.display_price === "display_as_last_price"
    )
}

    {if $product.price|floatval || $product.zero_price_action == "P" || ($hide_add_to_cart_button == "Y" && $product.zero_price_action == "A")}
        <span class="ty-price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">

            {if $product.is_discontinued === "YesNo::YES"|enum && $settings.product_discontinued.general.display_price === "display_as_last_price"}
                {__("last_price")}:
            {/if}

            {include file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="ty-price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}
        </span>
        {elseif $product.zero_price_action == "A" && $show_add_to_cart}
        {assign var="base_currency" value=$currencies[$smarty.const.CART_PRIMARY_CURRENCY]}
        <span class="ty-price-curency"><span class="ty-price-curency__title">{__("enter_your_price")}:</span>
        <div class="ty-price-curency-input">
            {if $base_currency.after != "Y"}{$base_currency.symbol nofilter}{/if}
            <input class="ty-price-curency__input" type="text" size="3" name="product_data[{$obj_id}][price]" value="" />
            {if $base_currency.after == "Y"}{$base_currency.symbol nofilter}{/if}
        </div>
        </span>

        {elseif $product.zero_price_action == "R"}
        <span class="ty-no-price">{__("contact_us_for_price")}</span>
        {assign var="show_qty" value=false}
    {/if}

{else}
    &nbsp;
{/if}

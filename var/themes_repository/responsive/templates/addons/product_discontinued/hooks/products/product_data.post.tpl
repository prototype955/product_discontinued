{if $product.is_discontinued === "YesNo::YES"|enum && $settings.product_discontinued.general.display_price === "no_display"}

    {capture name="old_price_`$obj_id`"}
        <!-- hidden by product_discontinued addons -->
    {/capture}

    {capture name="price_`$obj_id`"}
        <!-- hidden by product_discontinued addons -->
    {/capture}

    {capture name="clean_price_`$obj_id`"}
        <!-- hidden by product_discontinued addons -->
    {/capture}

    {capture name="list_discount_`$obj_id`"}
        <!-- hidden by product_discontinued addons -->
    {/capture}

    {capture name="discount_label_`$obj_prefix``$obj_id`"}
        <!-- hidden by product_discontinued addons -->
    {/capture}

{/if}

{if $product.is_discontinued === "YesNo::YES"|enum}

{capture name="product_labels_`$obj_prefix``$obj_id`"}
    
    {capture name="capture_product_labels_`$obj_prefix``$obj_id`"}

        {if $settings.product_discontinued.general.display_img_label === "YesNo::YES"|enum}
            {include
                file="views/products/components/product_label.tpl"
                label_meta="ty-product-labels__item--shipping"
                label_text=__("discontinued_product")
                label_mini=$product_labels_mini
                label_static=$product_labels_static
                label_rounded=$product_labels_rounded
            }
        {/if}

        {if $settings.product_discontinued.general.display_price !== "no_display"}
            {$smarty.capture.$capture_product_labels nofilter}
        {else}
            <!-- hidden by product_discontinued addons -->
        {/if}
    {/capture}

    {if $smarty.capture.$capture_product_labels|trim}
        <div class="ty-product-labels ty-product-labels--{$product_labels_position} {if $product_labels_mini}ty-product-labels--mini{/if} {if $product_labels_static}ty-product-labels--static{/if} cm-reload-{$obj_prefix}{$obj_id}" id="product_labels_update_{$obj_prefix}{$obj_id}">
            {$smarty.capture.$capture_product_labels nofilter}
        <!--product_labels_update_{$obj_prefix}{$obj_id}--></div>
    {/if}

{/capture}


{capture name="buttons_product"}
    <!-- hidden by product_discontinued addons -->
{/capture}

{/if}

{if $product.is_discontinued !== "YesNo::YES"|enum}

    {if $min_qty && $product.min_qty}
        <p class="ty-min-qty-description">{__("text_cart_min_qty", ["[product]" => $product.product, "[quantity]" => $product.min_qty])}.</p>
    {/if}

    {else}
    &nbsp;
{/if}
{if $product.is_discontinued !== "YesNo::YES"|enum}

    {if $disable_ids}
        {assign var="_disable_ids" value="`$disable_ids``$obj_id`"}
    {else}
        {assign var="_disable_ids" value=""}
    {/if}
    {include file="views/products/components/product_options.tpl" id=$obj_id product_options=$product.product_options name="product_data" capture_options_vs_qty=$capture_options_vs_qty disable_ids=$_disable_ids}
    
    {else}
    <!-- hidden by product_discontinued addons -->
{/if}

<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

$schema['export_fields']['Product discontinued'] = [
    'db_field' => 'is_discontinued',
    'process_put' => ['fn_product_discontinued_exim_import_discontinued_status', '#this', '#row', '#key'],
    'return_result' => true,
    'return_field'  => 'YML Exclude from prices'
];

$schema['export_fields']['Discontinue cause'] = [
    'db_field' => 'discontinue_cause'
];

$schema['export_fields']['Discontinue date'] = [
    'db_field' => 'discontinue_date',
    'process_get'   => ['fn_exim_get_optional_timestamp', '#this'],
    'convert_put'   => ['fn_exim_put_optional_timestamp', '#this'],
    'return_result' => true
];

$schema['export_fields']['Enable suscribtion if discontinue'] = [
    'db_field' => 'discontinue_action'
];

return $schema;
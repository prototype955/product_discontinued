<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\YesNo;

/**
 * Hook handler: parse discontinue data column
 */
function fn_product_discontinued_update_product_pre(&$product_data, $product_id, $lang_code, $can_update)
{
    if (!empty($product_data["discontinue_date"]) && !is_numeric($product_data["discontinue_date"])) {
        $product_data["discontinue_date"] = fn_parse_date($product_data["discontinue_date"]);
    }

    if ($product_data['is_discontinued'] === YesNo::YES) {

        $product_data = fn_product_discontinued_add_product_to_yml_price_lists($product_data);
    }

    $old_product_data = fn_product_discontinued_get_discontinued_data($product_id);

    if (!empty($product_id) && $old_product_data === YesNo::YES && $product_data['is_discontinued'] === YesNo::NO) {
        
        $product_data = fn_product_discontinued_remove_product_from_yml_price_lists($product_data);
    }
}

/**
 * Hook handler: stop add to cart process if discontinued
 */
function fn_product_discontinued_pre_add_to_cart(&$product_data, &$cart, &$auth, &$update)
{
    foreach ($product_data as $key => $entry) {

        $product_id = !empty($entry["product_id"]) ? $entry["product_id"] : $key;

        $product = fn_product_discontinued_get_discontinued_data($product_id);

        if ($product["is_discontinued"] === YesNo::YES) {
            fn_set_notification("E", __("notice"), __("product_has_been_discontinued"));
            unset($product_data[$key]);
            return false;
        }
    }

    return true;
}

/**
 * Hook handler: stop send product notifications if product is discontinued
 */
function fn_product_discontinued_send_product_notifications_before_fetch_subscriptions(&$product_id, $fields)
{
    $product = fn_product_discontinued_get_discontinued_data($product_id);
    
    if ($product["is_discontinued"] === YesNo::YES) {
        $product_id = 0;
    }
}

/**
 * Hook handler: remove discontinued products in cart
 */
function fn_product_discontinued_calculate_cart_items(&$cart, &$cart_products, &$auth)
{
    if (empty($cart["products"])) {
        return true;
    }

    foreach ($cart["products"] as $key => $entry) {

        if (empty($entry["product_id"])) {
            continue;
        }

        $product = fn_product_discontinued_get_discontinued_data($entry["product_id"]);

        if ($product["is_discontinued"] === YesNo::YES) {

            list($cart, $cart_products) = fn_product_discontinued_remove_product_from_cart($entry, $key, $cart, $cart_products);
        }
    }
}

/**
 * Hook handler: remove product from sitemap link is product discontinued
 */
function fn_product_discontinued_sitemap_link(&$link, $type, $id, $languages, &$links)
{
    if ($type == "product" && Registry::get("addons.product_discontinued.remove_from_sitemapxml") === YesNo::YES) {

        $product = fn_product_discontinued_get_discontinued_data($id);
    
        if ($product["is_discontinued"] === YesNo::YES) {
            $links = array();
        }
    }
}

/**
 * Hook handler: hide discontinued products from shop blocks and catalog
 */
function fn_product_discontinued_get_products(
    $params, 
    &$fields, 
    $sortings, 
    &$condition, 
    $join, 
    $sorting, 
    $group_by, 
    $lang_code, 
    $having
)
{
    if (
        (AREA == "C" && Registry::get("addons.product_discontinued.hide_from_public") === YesNo::YES) 
        || 
        isset($params["is_discontinued"])
    ) {
        $is_discontinued = isset($params["is_discontinued"]) ? $params["is_discontinued"] : YesNo::YES;
        $fields["is_discontinued"] = "products.is_discontinued as is_discontinued";
        $condition .= db_quote(" AND is_discontinued != ?s ", $is_discontinued);
    }
}

/**
 * Hook handler: setting for addon data_feeds - exclude discontinued products
 */
function fn_product_discontinued_data_feeds_export_before_get_products($datafeed_data, $pattern, &$params)
{
    if ($datafeed_data["params"]["exclude_discontinued_products"] === YesNo::YES) {
        $params["is_discontinued"] = YesNo::YES;
    }
}

/**
 * Removes product from cart 
 *
 * @param array  $product Array of the product content (only cart data)
 * @param int    $key Integer key of the product in cart
 * @param array  $cart Array of the cart contents and user information necessary for purchase
 * @param array  $cart_products Cart products
 * @return array Handled $cart and $cart_products in array
 */
function fn_product_discontinued_remove_product_from_cart($product, $key, $cart, $cart_products)
{
    if (empty($product["extra"]["parent"])) {
        $cart["amount"] -= $product["amount"];
    }

    unset($cart["products"][$key]);
    unset($cart_products[$key]);

    if (isset($cart["product_groups"])) {

        foreach ($cart["product_groups"] as $key_group => $group) {

            if (in_array($key, array_keys($group["products"]))) {
                unset($cart["product_groups"][$key_group]["products"][$key]);
            }
        }
    }

    return array($cart, $cart_products);
}

/**
 * Gets discontinued data of a product
 *
 * @param int    $product_id Product identifier
 * @return array Discontinued data
 */
function fn_product_discontinued_get_discontinued_data($product_id)
{
    $data = db_get_row(
        "SELECT is_discontinued, discontinue_cause, discontinue_date, discontinue_action"
        . " FROM ?:products"
        . " WHERE product_id = ?i",
        $product_id
    );

    return $data;
}

/**
 * Gets discontinued products
 *
 * @param string $condition_sql Condition for sql request
 * @return array Discontinued products
 */
function fn_product_discontinued_get_discontinued_products($condition_sql = "")
{
    $data = db_get_hash_array(
        "SELECT * "
        . " FROM ?:products WHERE is_discontinued = ?s ?p",
        "product_id",
        YesNo::YES,
        $condition_sql
    );

    return $data;
}

/**
 * Adds product to yml price lists
 *
 * @param array $product_data Product data
 * @return array Discontinued products
 */
function fn_product_discontinued_add_product_to_yml_price_lists($product_data)
{
    $price_listes = fn_yml_get_price_lists();

    if (empty($price_listes)) {
        return $product_data;
    }

    foreach ($price_listes as $price) {

        if ($price["param_data"]["exclude_discontinued_products"] !== YesNo::YES) {
            continue;
        }

        if (!isset($product_data['yml2_exclude_price_ids']) || !is_array($product_data['yml2_exclude_price_ids'])) {
            $product_data['yml2_exclude_price_ids'] = array();  
        }

        if (!in_array($price["param_id"], $product_data['yml2_exclude_price_ids'])) {
            $product_data['yml2_exclude_price_ids'][] = $price["param_id"];
        }
    }

    return $product_data;
}

/**
 * Remove product from yml price lists
 *
 * @param array $product_data Product data
 * @return array Discontinued products
 */
function fn_product_discontinued_remove_product_from_yml_price_lists($product_data)
{
    $price_listes = fn_yml_get_price_lists();

    if (empty($price_listes)) {
        return $product_data;
    }

    foreach ($price_listes as $price) {

        if ($price["param_data"]["exclude_discontinued_products"] !== YesNo::YES) {
            continue;
        }

        if (!isset($product_data['yml2_exclude_price_ids']) || !is_array($product_data['yml2_exclude_price_ids'])) {
            break;  
        }

        $key = array_search($price["param_id"], $product_data['yml2_exclude_price_ids']);

        if ($key !== false) {
            unset($product_data['yml2_exclude_price_ids'][$key]);
        }
    }

    return $product_data;
}

function fn_product_discontinued_exim_import_discontinued_status($value, $row, $key)
{
    if (isset($row['YML Exclude from prices'])) {
        $row["yml2_exclude_price_ids"] = explode(",", $row['YML Exclude from prices']);
    } else {
        $row["yml2_exclude_price_ids"] = array();
        $product_id = fn_product_discontinued_find_product_id_by_product_code($row["product_code"], $row["company_id"]);

        if (!empty($product_id)) {
            $exclude_products = fm_yml_get_exclude_products($product_id);

            foreach ($exclude_products as $exclude_product) {

                if ($exclude_product["object_type"] !== "product") {
                    continue;
                }

                $row["yml2_exclude_price_ids"][] = $exclude_product["price_id"];
            }
        }
    }

    if ($value === YesNo::YES) {
        
        $row = fn_product_discontinued_add_product_to_yml_price_lists($row);

    } else {
        $row = fn_product_discontinued_remove_product_from_yml_price_lists($row);
    }

    $imploded_exclude_prices = implode(",", $row['yml2_exclude_price_ids']);

    fn_yml_import_exclude_prices($key, $imploded_exclude_prices);
    
    return $imploded_exclude_prices;
}

/**
 * Fing product id by code
 *
 * @param string $product_code Product code
 * @param int    $company_id Product company
 * @return int Product id
 */
function fn_product_discontinued_find_product_id_by_product_code($product_code, $company_id)
{
    return (int) db_get_field(
        'SELECT product_id FROM ?:products WHERE product_code = ?s AND company_id = ?i',
        $product_code,
        $company_id
    );
}

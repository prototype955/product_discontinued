<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\YesNo;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$request = $_REQUEST;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update') {
        
        $price_id = $request['price_id'];

        if (!empty($request['pricelist_data'])) {
            fn_product_discontinued_yml_update_price_list($price_id, $request['pricelist_data']);
        }
    }
}

function fn_product_discontinued_yml_update_price_list($price_id, $pricelist_data)
{
    $company_id = fn_get_runtime_company_id();

    $access_key = isset($pricelist_data["access_key"]) ? $pricelist_data["access_key"] : '';

    $price_id = empty($price_id) ? fn_yml_get_price_id($access_key) : $price_id;

    $price = fn_yml_get_price_list($price_id);

    if ($price['company_id'] != $company_id) {
        return false;
    }

    $discontinued_products = fn_product_discontinued_get_discontinued_products();

    if (empty($discontinued_products)) {
        return false;
    }

    if ($pricelist_data["exclude_discontinued_products"] === YesNo::YES) {

        $sql_insert_fields = array("price_id", "object_id", "object_type");
        $sql_insert_values = array();

        foreach ($discontinued_products as $product) {
            $sql_insert_values[] = db_quote(' (?i, ?i, ?s) ', $price_id, $product["product_id"], "product");
        }

        db_query("REPLACE INTO ?:yml_exclude_objects(?p) VALUES ?p", implode(",", $sql_insert_fields), implode(",", $sql_insert_values));

    } elseif ($pricelist_data["exclude_discontinued_products"] === YesNo::NO) {

        $products_ids = array_keys($discontinued_products);

        db_query("DELETE FROM ?:yml_exclude_objects WHERE price_id = ?i AND object_id IN (?a) AND object_type = ?s", $price_id, $products_ids, "product");
    }

    return true;
}

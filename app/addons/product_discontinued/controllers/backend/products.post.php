<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\YesNo;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $request = $_REQUEST;

    if ($mode == 'update' && isset($request["alternative_product_send"]) && $request["alternative_product_send"] === YesNo::YES) {
        
        $result = fn_product_discontinued_send_alternative_product($request);

        $product_id = isset($request['product_id']) ? (int) $request['product_id'] : 0;

        if (!empty($product_id) && $result !== false && $request["alternative_product"]["remove_subscribers_after_send"] === YesNo::YES && !defined('ORDER_MANAGEMENT')) {

            db_query('DELETE FROM ?:product_subscriptions WHERE product_id = ?i', $product_id);
        }
    }
}

function fn_product_discontinued_send_alternative_product($request)
{
    $product_id = isset($request['product_id']) ? (int) $request['product_id'] : 0;

    if (!empty($product_id) && isset($request["alternative_product"]["product"]) && !empty($request["alternative_product"]["product"])) {

        $fields = array(
            'email' => 'email',
        );

        $subscriptions = fn_product_discontinued_get_subscribtions_for_product($product_id, $fields);
        $alternative_product_id = $request["alternative_product"]["product"];

        if (!empty($subscriptions)) {

            fn_product_discontinued_send_alternative_product_mail($subscriptions, $product_id, $alternative_product_id, $fields);

            return true;
        }
    }

    return false;
}   

function fn_product_discontinued_get_subscribtions_for_product($product_id, $fields)
{
    if (empty($product_id)) {
        return false;
    }

    $query_fields = implode(', ', $fields);
    $subscriptions_data = db_get_array('SELECT ?p FROM ?:product_subscriptions WHERE product_id = ?i', $query_fields, $product_id);

    return $subscriptions_data;
} 

function fn_product_discontinued_send_alternative_product_mail($subscriptions_data, $product_id, $alternative_product_id, $params)
{
    $alternative_product = array();
    $alternative_product["name"] = fn_get_product_name($alternative_product_id, Registry::get('settings.Appearance.frontend_default_language'));

    $product = array();
    $product["name"] = fn_get_product_name($product_id, Registry::get('settings.Appearance.frontend_default_language'));

    $product_company_id = fn_get_company_id('products', 'product_id', $alternative_product_id);
    $subscribers_by_companies = array();

    if (isset($params['company_id'])) {

        foreach ($subscriptions_data as $subscription) {
            $company_id = !empty($subscription['company_id']) ? $subscription['company_id'] : $product_company_id;
            $subscribers_by_companies[$company_id][] = $subscription['email'];
        }

    } else {
        $subscribers_by_companies[$product_company_id] = fn_array_column($subscriptions_data, 'email');
    }

    /** @var \Tygh\Mailer\Mailer $mailer */
    $mailer = Tygh::$app['mailer'];

    foreach ($subscribers_by_companies as $company_id => $subscribers) {

        if (empty($subscribers)) {
            continue;
        }

        $suffix = '';

        if (fn_allowed_for('ULTIMATE')) {
            $suffix = '&company_id=' . $company_id;
        }

        $result = $mailer->send(array(
            'to' => $subscribers,
            'from' => 'company_orders_department',
            'reply_to' => 'company_orders_department',
            'data' => array(
                'dear' => __("dear"),
                'customer' => __("customer"),
                'product_name' => $product["name"],
                'product_id' => $product_id,
                'alt_product_name' => $alternative_product["name"],
                'alt_product_id' => $alternative_product_id,
                'product_url' => fn_url("products.view?product_id=$product_id" . $suffix, 'C', 'http'),
                'alt_product_url' => fn_url("products.view?product_id=$alternative_product_id" . $suffix, 'C', 'http'),
            ),
            'template_code' => 'product_discontinued_send_alternative',
            'tpl' => 'addons/product_discontinued/product_discontinued_send_alternative.tpl', // this parameter is obsolete and is used for back compatibility
            'company_id' => $company_id,
        ), 'A', Registry::get('settings.Appearance.frontend_default_language'));
    }
} 
